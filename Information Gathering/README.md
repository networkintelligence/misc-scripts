# Contents #

This file gives a short description of the scripts that are available in this folder for better understanding of the script's purpose.

* exploit_serach.py - This script runs nmap syn scan with version detection and then uses searchsploit to list the available exploits against the technologies / versions detected. (Credits: Sumit Shrivastava)
* nepture_ip_port_scanner_enum.py - This scripts runs all the basic port scans that is required for the information gathering phase and publishes it on a webserver to download. (Credits: Varun Kaundal)